# Release Notes
2024-12-07

## Added

* oml-659: Upgrade to Rtpengine mr13.0.1.5. New Dockerfile multistage improve container img size.

## Changed

## Fixed

## Removed