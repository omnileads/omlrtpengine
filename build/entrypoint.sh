#!/bin/bash
set -e
PATH=/usr/local/bin:$PATH

if [[ -z "${PUBLIC_IP}" ]]; then
    PUBLIC_IP=$(curl http://ipinfo.io/ip)
fi

  case ${ENV} in
  devenv)
    echo "devenv docker-compose"
    MY_IP_MEDIA=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'`
    MY_IP_NG=$MY_IP_MEDIA
    ;;
  docker)
    echo "proenv docker-compose"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=0.0.0.0
    if [[ "${SIP_NAT_MODE}" == "public_ip" ]]; then
      MY_IP_MEDIA="external\/${RTPENGINE_HOSTNAME}!${PUBLIC_IP}"
    elif  [[ "${SIP_NAT_MODE}" == "lan_ip" ]]; then  
      MY_IP_MEDIA="external\/${RTPENGINE_HOSTNAME}!${PRIVATE_IP}"
    else
      echo "Error No NAT mode selected"
      echo "Posible NAT audio problems"
    fi
    ;;
  cloud)
    echo "cloud"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA="internal\/${RTPENGINE_HOSTNAME}\;external\/${PUBLIC_IP}"
    ;;
  cloud_external_dialer)
    echo "cloud"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA="internal\/${RTPENGINE_HOSTNAME}\;external\/${PUBLIC_IP}"
    ;;  
  lan)
    echo "lan"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf    
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA=${RTPENGINE_HOSTNAME}    
    ;;
  nat)
    echo "nat"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA="external\/${RTPENGINE_HOSTNAME}!${PUBLIC_IP}"    
    ;;
  hybrid)
    echo "hybrid"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA="external\/${RTPENGINE_HOSTNAME}!${PUBLIC_IP}"    
    ;;  
  all)
    echo "all"
    sed -i -e "s/20100/30000/g" /etc/rtpengine.conf
    MY_IP_NG=${RTPENGINE_HOSTNAME}
    MY_IP_MEDIA="internal\/${RTPENGINE_HOSTNAME}\;external\/${PUBLIC_IP}"    
    ;;  
  *)
    echo "You must to pass ENV var: devenv"
    ;;
  esac

# Set Scale parameters
if [[ -n $SCALE && $SCALE == "True" ]]; then
  sed -i -e "s/#timeout=.*/timeout=${TIMEOUT:-60}/g" /etc/rtpengine.conf
  sed -i -e "s/#offer-timeout=.*/offer-timeout=${OFFER_TIMEOUT:-120}/g" /etc/rtpengine.conf
  sed -i -e "s/#silent-timeout=.*/silent-timeout=${SILENT_TIMEOUT:-120}/g" /etc/rtpengine.conf
  sed -i -e "s/#final-timeout=.*/final-timeout=${FINAL_TIMEOUT:-3600}/g" /etc/rtpengine.conf
  sed -i -e "s/30000/39999/g" /etc/rtpengine.conf
fi

sed -i -e "s/MY_IP_MEDIA/$MY_IP_MEDIA/g" /etc/rtpengine.conf
sed -i -e "s/MY_IP_NG/$MY_IP_NG/g" /etc/rtpengine.conf
exec rtpengine --config-file /etc/rtpengine.conf  "$@"
