# Etapa de desarrollo
FROM debian:12.1 as dev

ENV LANG en_US.UTF-8
ENV NOTVISIBLE "in users profile"
ENV DEBIAN_FRONTEND=noninteractive

# Actualizar e instalar dependencias esenciales
RUN apt-get update \
    && apt-get install -y locales build-essential git pkg-config \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Clonar el repositorio en la versión requerida
RUN git clone --depth 1 --branch mr13.0.1.5 https://github.com/sipwise/rtpengine.git rtpengine

# Instalar dependencias de construcción
WORKDIR /rtpengine
RUN apt-get update \
    && apt-get -y build-dep -Ppkg.ngcp-rtpengine.nobcg729 . \
    && apt-get install -y \
        libpcap-dev \
        libspandsp-dev \
        libswresample-dev \
        liburing-dev \
        libwebsockets-dev \
        libxmlrpc-core-c3-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Construir los paquetes .deb necesarios
RUN dpkg-buildpackage -Ppkg.ngcp-rtpengine.nobcg729

# Etapa de producción
FROM debian:bookworm-slim as run

ENV DEBIAN_FRONTEND=noninteractive

# Instalar solo las dependencias necesarias
RUN apt-get update \
    && apt-get install -y locales curl iproute2 \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Copiar el paquete compilado desde la etapa de desarrollo
COPY --from=dev /ngcp-rtpengine-daemon_*.deb /tmp

# Instalar dependencias de rtpengine
RUN apt-get update \
    && apt-get install -y \
        libpcap0.8 \
        libspandsp2 \
        libswresample4 \
        liburing2 \
        libwebsockets17 \
        libxmlrpc-core-c3 \
        libopus0 \
        libavcodec59 \
        libavformat59 \
        libevent-2.1-7 \
        libevent-pthreads-2.1-7 \
        libglib2.0-0 \
        libhiredis0.14 \
        libip4tc2 \
        libip6tc2 \
        libjson-glib-1.0-0 \
        libmariadb3 \
        libmosquitto1 \
        libnftnl11 \
        curl \
    && dpkg -i /tmp/ngcp-rtpengine-daemon_*.deb \
    && rm -v /tmp/ngcp-rtpengine-daemon_*.deb \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Configuración y archivos de entrada
COPY build/entrypoint.sh /
COPY build/rtpengine.conf /etc

CMD ["/entrypoint.sh"]
