#!/bin/bash

########################## README ############ README ############# README #########################
########################## README ############ README ############# README #########################
# El script first_boot_installer tiene como finalidad desplegar el componente sobre una instancia
# de linux exclusiva. Las variables que utiliza son "variables de entorno" de la instancia que está
# por lanzar el script como acto seguido al primer boot del sistema operativo.
# Dichas variables podrán ser provisionadas por un archivo .env (ej: Vagrant) o bien utilizando este
# script como plantilla de terraform.
#
# En el caso de necesitar ejecutar este script manualmente sobre el user_data de una instancia cloud
# o bien sobre una instancia onpremise a través de una conexión ssh, entonces se deberá copiar
# esta plantilla hacia un archivo ignorado por git: first_boot_installer.sh para luego sobre
# dicha copia descomentar las líneas que comienzan con la cadena "export" para posteriormente
# introducir el valor deseado a cada variable.
########################## README ############ README ############# README #########################
########################## README ############ README ############# README #########################

# *********************************** SET ENV VARS **************************************************
# *********************************** SET ENV VARS **************************************************

# The infrastructure environment:
# onpremise | digitalocean | linode | vultr | aws
#export oml_infras_stage=onpremise

# Set your net interfaces, you must have at least a PRIVATE_NIC
# The public interface is not mandatory, if you don't have it, you can leave it blank
#export oml_nic=eth1
#export oml_public_nic=eth0

# Component gitlab branch
#export oml_rtpengine_release=210629.01

########################################## STAGE #######################################
# You must to define your scenario to deploy RTPEngine
# LAN if all agents work on LAN netwrok or VPN
# CLOUD if all agents work from the WAN
# HYBRID_1_NIC if some agents work on LAN and others from WAN and the host have ony 1 NIC
# HYBRID_2_NIC if some agents work on LAN and others from WAN and the host have 2 NICs
# (1 NIC for LAN IPADDR and 1 NIC for WAN IPADDR)
#export oml_type_deploy=

# *********************************** SET ENV VARS **************************************************
# *********************************** SET ENV VARS **************************************************

COMPONENT_REPO_URL=https://gitlab.com/omnileads/omlrtpengine.git
COMPONENT_REPO_DIR=omlrtpengine
SRC=/usr/src

# *********************************** SET ENV VARS **********************************************

SRC=/usr/src
COMPONENT_REPO=https://gitlab.com/omnileads/omlkamailio.git

echo "******************** IPV4 address config ***************************"
echo "******************** IPV4 address config ***************************"

PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
PRIVATE_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)

apt update && apt install -y ansible git

cd $SRC
git clone $COMPONENT_REPO_URL
cd $COMPONENT_REPO_DIR
git checkout ${oml_rtpengine_release}
cd deploy

sed -i "s/omni_ip=/omni_ip=$PRIVATE_IPV4/g" ./inventory

ansible-playbook rtpengine.yml -i inventory --extra-vars "rtpengine_version=$(cat ../.rtpengine_version)"
